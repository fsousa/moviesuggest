-- phpMyAdmin SQL Dump
-- version 3.3.2deb1ubuntu1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 10, 2012 at 03:40 PM
-- Server version: 5.1.63
-- PHP Version: 5.3.2-1ubuntu4.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sysrec`
--

-- --------------------------------------------------------

--
-- Table structure for table `moive_actor`
--

CREATE TABLE IF NOT EXISTS `moive_actor` (
  `id_movie` int(8) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id_movie`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `moive_actor`
--


-- --------------------------------------------------------

--
-- Table structure for table `movie`
--

CREATE TABLE IF NOT EXISTS `movie` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `ano` int(4) NOT NULL,
  `resume` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `movie`
--

INSERT INTO `movie` (`id`, `name`, `ano`, `resume`) VALUES
(1, 'The Expendables 2 ', 2012, 'Mr. Church reunites the Expendables for what should be an easy paycheck, but when one of their men is murdered on the job, their quest for revenge puts them deep in enemy territory and up against an unexpected threat. ');

-- --------------------------------------------------------

--
-- Table structure for table `movie_director`
--

CREATE TABLE IF NOT EXISTS `movie_director` (
  `id_movie` int(8) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id_movie`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `movie_director`
--

INSERT INTO `movie_director` (`id_movie`, `name`) VALUES
(1, 'Simon West');

-- --------------------------------------------------------

--
-- Table structure for table `movie_gener`
--

CREATE TABLE IF NOT EXISTS `movie_gener` (
  `id_movie` int(8) NOT NULL,
  `id_tag` int(8) NOT NULL,
  PRIMARY KEY (`id_movie`,`id_tag`),
  KEY `id_tag` (`id_tag`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `movie_gener`
--

INSERT INTO `movie_gener` (`id_movie`, `id_tag`) VALUES
(1, 1),
(1, 24);

-- --------------------------------------------------------

--
-- Table structure for table `tag`
--

CREATE TABLE IF NOT EXISTS `tag` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `tag`
--

INSERT INTO `tag` (`id`, `name`) VALUES
(1, 'Action'),
(2, 'Adventure'),
(3, 'Animation'),
(4, ' Biography'),
(5, 'Comedy'),
(6, ' Crime'),
(7, ' Documentary'),
(8, 'Drama'),
(9, 'Family'),
(10, 'Fantasy'),
(11, ' Film-Noir'),
(12, 'Game-Show'),
(13, 'History'),
(14, 'Horror'),
(15, ' Music'),
(16, ' Musical'),
(17, 'Mystery'),
(18, ' News'),
(19, ' Reality-TV'),
(20, 'Romance'),
(21, 'Sci-Fi'),
(22, ' Sport'),
(23, ' Talk-Show'),
(24, ' Thriller'),
(25, 'War'),
(26, ' Western');

-- --------------------------------------------------------

--
-- Table structure for table `tag_user`
--

CREATE TABLE IF NOT EXISTS `tag_user` (
  `id_user` varchar(50) NOT NULL,
  `id_tag` int(8) NOT NULL,
  `crt` int(8) NOT NULL,
  PRIMARY KEY (`id_user`,`id_tag`),
  KEY `id_tag` (`id_tag`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tag_user`
--


-- --------------------------------------------------------

--
-- Table structure for table `tid`
--

CREATE TABLE IF NOT EXISTS `tid` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `id_user` varchar(50) NOT NULL,
  `id_movie` int(8) NOT NULL,
  PRIMARY KEY (`id`,`id_user`,`id_movie`),
  KEY `id_user` (`id_user`),
  KEY `id_movie` (`id_movie`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tid`
--

INSERT INTO `tid` (`id`, `id_user`, `id_movie`) VALUES
(1, 'felipe.sousa.f.s.s@gmail.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tid_result`
--

CREATE TABLE IF NOT EXISTS `tid_result` (
  `tid_id` int(8) NOT NULL,
  `rule` int(8) NOT NULL,
  `sup` float NOT NULL,
  `conf` float NOT NULL,
  PRIMARY KEY (`tid_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tid_result`
--


-- --------------------------------------------------------

--
-- Table structure for table `tid_tag_list`
--

CREATE TABLE IF NOT EXISTS `tid_tag_list` (
  `tid_id` int(8) NOT NULL,
  `tag_id` int(8) NOT NULL,
  PRIMARY KEY (`tid_id`,`tag_id`),
  KEY `tag_id` (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tid_tag_list`
--

INSERT INTO `tid_tag_list` (`tid_id`, `tag_id`) VALUES
(1, 5),
(1, 20);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `email` varchar(50) NOT NULL,
  `name` varchar(30) NOT NULL,
  `pass` int(12) NOT NULL,
  `birthday` date NOT NULL,
  `sexo` int(1) NOT NULL,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`email`, `name`, `pass`, `birthday`, `sexo`) VALUES
('felipe.sousa.f.s.s@gmail.com', 'Felipe Sousa', 123456, '1991-03-18', 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `moive_actor`
--
ALTER TABLE `moive_actor`
  ADD CONSTRAINT `moive_actor_ibfk_1` FOREIGN KEY (`id_movie`) REFERENCES `movie` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `movie_director`
--
ALTER TABLE `movie_director`
  ADD CONSTRAINT `movie_director_ibfk_1` FOREIGN KEY (`id_movie`) REFERENCES `movie` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `movie_gener`
--
ALTER TABLE `movie_gener`
  ADD CONSTRAINT `movie_gener_ibfk_1` FOREIGN KEY (`id_movie`) REFERENCES `movie` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `movie_gener_ibfk_2` FOREIGN KEY (`id_tag`) REFERENCES `tag` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tag_user`
--
ALTER TABLE `tag_user`
  ADD CONSTRAINT `tag_user_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`email`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tag_user_ibfk_2` FOREIGN KEY (`id_tag`) REFERENCES `tag` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tid`
--
ALTER TABLE `tid`
  ADD CONSTRAINT `tid_ibfk_2` FOREIGN KEY (`id_movie`) REFERENCES `movie` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tid_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`email`) ON UPDATE CASCADE;

--
-- Constraints for table `tid_tag_list`
--
ALTER TABLE `tid_tag_list`
  ADD CONSTRAINT `tid_tag_list_ibfk_2` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tid_tag_list_ibfk_1` FOREIGN KEY (`tid_id`) REFERENCES `tid` (`id`) ON UPDATE CASCADE;
