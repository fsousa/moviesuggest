require("display")

function init(evt)
	local res, msg = pcall(handler, evt)

	if (not res) then
        	print(msg)
	end

end

function handler(evt)
	if evt.class == 'ncl' and evt.type == 'presentation' and evt.action == 'start' then
		startSearch()
	end
	if (evt.class == 'key' and evt.type == 'press') then 
		if (evt.key == 'CURSOR_UP') then
			reciveEvent("UP")
		elseif (evt.key == 'CURSOR_DOWN') then
			reciveEvent("DW")
		elseif (evt.key == 'ENTER') then
			reciveEvent("ET")
		end
	end

end

event.register(init)


