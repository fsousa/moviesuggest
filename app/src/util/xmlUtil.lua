dofile("LuaXML/xml.lua")
dofile("LuaXML/handler.lua")

local XMLFOLDER = "../res/xmlFolder/"

function openXmlFile(xmlPath)
	local xml = assert(io.open(xmlPath,"r"))
	local output = xml:read("*all")
	xml:close()
	return output
end

function saveXmlFile(fileName, xmlText)
	local file = assert(io.open(XMLFOLDER..fileName..".xml","w"))
	file:write(xmlText)
	file:close()
end

function convertXmltvDate(time)
	
	local pYear = string.sub(time,1,4)
	local pMon = string.sub(time,5,6)
	local pDay = string.sub(time,7,8)
	
	local pHour = string.sub(time,9,10)
	local pMin = string.sub(time,11,12)
	local pSec = string.sub(time,13,14)
	
	local tblTime = {}
	tblTime.year = pYear
	tblTime.month = pMon
	tblTime.day = pDay
	tblTime.hour = pHour
	tblTime.min = pMin
	tblTime.sec = pSec
	return tblTime
	
end

function myXmlParser(xmlText)
	local xmlhandler = simpleTreeHandler()
	local xmlparser = xmlParser(xmlhandler)
	xmlparser:parse(xmlText)
	return xmlhandler.root
end

