function toISO (s)
	if string.find(s, "[\224-\255]") then
		--error("non-ISO char")
		return s
	end
	s = string.gsub(s, "([\192-\223])(.)", function (c1, c2)
	c1 = string.byte(c1) - 192
	c2 = string.byte(c2) - 128
	return string.char(c1 * 64 + c2)
	end)
	return s
end

function fromISO88591toUTF8(text)
	if (not string.find(text, "[\224-\255]")) then
		--error("non-ISO char")
		return text
	end
	text = string.gsub(text, "([\160-\255])", function (c1)
		bc1 = string.byte(c1)
		if (bc1 >= 192) then
			return string.char(195)..string.char(128 + bc1%64)
		else
			return string.char(194)..string.char(128 + bc1%64)
		end
		end)
	return text
end

--print("------------------------- CORREÇÃO ---------------------------")

--texto = "O EW acão &xóta aaaaaçvvã Áff ¢ff"
--texto = "O Diário da Princesa"
--texto = toISO(texto)
--print('toIso: '..texto)

--corrige = fromISO88591toUTF8(texto)
--print('\ntoUTF8: '..corrige)

--print("--------------------------------------------------------------")

--casos de Teste
