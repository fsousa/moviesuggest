local require, io, print, string = require, io, print, string

module("conn.remote")

local http = require("socket.http")
local API = "http://repositorium.dyndns.org/imdb/service.php?q="
local OUTPUTTYPE = "&o=xml"  

function strToUrl(str)
	local nameToUrl = string.gsub(str, " ", "+")
	return nameToUrl
end

function request(url)
	local nameToUrl = strToUrl(url)
	return http.request(nameToUrl)
end

function getMovieByName(name)
	local nameToUrl = strToUrl(name)
	local url = API..nameToUrl..OUTPUTTYPE
	local xmlResponse = http.request(url)
	return xmlResponse
end

