mock = {}

require("config")


function openFile(path)
	local file = assert(io.open(path, "r"))
	local out = file:read("*all")
	file:close()
	return out

end

function mock.request(url)
	print(MOCK_FILE..string.sub(url,22))
	return openFile(MOCK_FILE..string.sub(url,22))
end


function mock.gerMovieByName(name)
	local nameToUrl = strToUrl(name)
	print("FILMEE",MOCK_FILE..nameToUrl)
	return MOCK_FILE..nameToUrl
end

return mock
