
local ROOT = "../"
local IMGPATH = "res/imgFolder/thumb/"
local XMLPATH = "res/xmlFolder/"

function split(str,sep)
	local ret={}
	local i=1
	repeat
		while str:find(sep,i,true)==i do i=i+sep:len() end
		if i>=str:len() then break end

		startPos=i
		endPos=str:find(sep,i,true)
		if endPos then
			element=str:sub(startPos,endPos-1)
			i=endPos
		else
			element=str:sub(startPos)
		end
		table.insert(ret,element)
	until endPos==nil

	return ret
end

function tprint (tbl,out)
	local out = out or {}
	if(tbl) then
		for k, v in pairs(tbl) do
			if type(v) == "table" then
				return tprint(v, out)
			else
				table.insert(out, v)
			end
		end
	else
		table.insert(out, "No data")
	end
	return out
end

function convertNameToPath(name)
	local outStr = string.gsub(name, " ", "+")
	return outStr
end

local imgCache = {}
local movieDetailsCache = {}
function loadCache()

	for i in io.popen("ls ./application/MovieSuggest/app/"..IMGPATH):lines() do
		imgCache[ROOT..IMGPATH..i] = ROOT..IMGPATH..i
	end
	for i in io.popen("ls ./application/MovieSuggest/app/"..XMLPATH):lines() do
		movieDetailsCache[ROOT..XMLPATH..i] = ROOT..XMLPATH..i	
	end
end

function downloadMoviePoster(url,name)
	local extension = string.sub(url,-4)
	local img = ROOT..IMGPATH..convertNameToPath(name)..extension

	print(img)
	if(imgCache[img]) then
		return img
	end
	
	local bytesImg = lib.request(url)
	local file = assert(io.open(img,"w"))
	file:write(bytesImg)
	file:close()
	imgCache[img] = img
	return img

end

function viewEmulator(xml)
	local movie = {}
	local listXmlObject = myXmlParser(xml)
	
	for k, p in pairs(listXmlObject) do
		movie.title = p.TITLE
		movie.year = p.YEAR
		movie.runtime = p.RUNTIME
		movie.story = p.STORYLINE
		movie.directors = tprint(p.DIRECTORS)
		movie.writers = tprint(p.WRITERS)
		movie.genres = tprint(p.GENRES)
		movie.rating = p.RATING
		movie.poster = downloadMoviePoster(p.POSTER,p.TITLE)
	end
	
	return movie
end

local started = false
function movieDescription(movieTitle)
	if(not started) then
		loadCache()
		started = true
	end
	
	if(movieDetailsCache[ROOT..XMLPATH..movieTitle..".xml"]) then
		print(movieTitle, "cache")
		return viewEmulator(openXmlFile(movieDetailsCache[ROOT..XMLPATH..movieTitle..".xml"]))
	end

	local movieResponseXml = lib.getMovieByName(movieTitle)
	movieDetailsCache[movieTitle] = movieResponseXml
	print(movieTitle, "SaveXML")
	saveXmlFile(movieTitle, movieResponseXml)
	return viewEmulator(movieResponseXml)
end

function emptyCache()
	imgCache = {}
	movieDetailsCache = {}
	os.execute("rm -f "..IMGPATH.."*")
	os.execute("rm -f "..XMLPATH.."*")
	started = false
end


