require("util.xmlUtil")
require("util.encode")

local ROOT = "../"
local epgPath = EPG_SERVER
local epgLocalPath = "res/epg/"
local epgEnd =  "xmltv.xml"

local epgBand = epgPath.."ban_"..epgEnd
local epgCult = epgPath.."cul_"..epgEnd
local epgFutu =  epgPath.."fut_"..epgEnd
local epgGjh =  epgPath.."gjh_"..epgEnd
local epgJust =  epgPath.."jus_"..epgEnd
local epgMtv =  epgPath.."mtv_"..epgEnd
local epgReco =  epgPath.."rec_"..epgEnd
local epgRth =  epgPath.."rth_"..epgEnd
local epgVda =  epgPath.."vda_"..epgEnd

local ChannelList = {}
ChannelList.Band = epgBand
ChannelList.Cultura = epgCult
ChannelList.Futura = epgFutu
ChannelList.Globo = epgGjh
ChannelList.Justica = epgJust
ChannelList.MTV = epgMtv
ChannelList.Record = epgReco
ChannelList.RedeTV = epgRth
ChannelList.RedeVida = epgVda


local cache = {}
local downloaded = false
local epgCache = {}

function isMovie (tbl)
	for k, v in pairs(tbl) do
		if type(v) == "table" then
			return isMovie(v)
		else
			if(v == "Filme" or v == "Cinema") then
				return true
			end
		end
	end
	return false
end

---Function to verify when a channel can be watched
--TODO : include GTM to calculate date
function isSchedulable(time)
	local currentTime = os.time()
	local tblTime = convertXmltvDate(time)
	
	--The compare signal is wrong because my xmltv are old.
	if(currentTime < os.time(tblTime)) then
		return false
	end
	return true
	
end

---Function to extract movie content of xmltv file.
function moviesToWatch(url)

	local output = {}
	local xmlText = lib.request(url)
	local xmlhandler = simpleTreeHandler()
	local xmlparser = xmlParser(xmlhandler)
	xmlparser:parse(xmlText)
	
	for k, p in pairs(xmlhandler.root.tv.programme) do
		if(isMovie(p.category)) then
			if (isSchedulable(p._attr.start)) then
				if(p.title[1][1] and p._attr.start) then
					local event = {}
					event.title = fromISO88591toUTF8(p.title[1][1])
					event.start = p._attr.start
					table.insert(output, event)
				end
			end
		end
	end
	return output
end


--- Function to search and cache movies
function searchMoviesOnMyChannels()
	if(not downloaded) then
		loadEpgCache()
		local movieList = {}
		for i,v in pairs(ChannelList) do
			if(i ~= nil and v ~= nil) then 
				movieList[i] = moviesToWatch(v)
			end
		end
		cache = movieList
		downloaded = true
		return movieList
	end
	return cache
end

function loadEpgCache()
	for i in io.popen("ls ./application/MovieSuggest/app/"..epgLocalPath):lines() do
		epgCache[epgPath..i] = ROOT..epgLocalPath..i
	end
end



---Empty cache when refesh button is pressed.
function emptyEpgCache() 
	downloaded = false
	cache = {}
end





