
require("config")

lib = nil

if(MOCK) then
	lib = require("conn.mock")
else
	lib = require("conn.remote")
end
require ("epgReader")
require ("imdbLib")
require ("footer")

local menu = {}
local menuDetails = {}
local currentPosition = 1
local defaultX = 20
local defaultY = 40

local WIDTH = 1280
local HEIGHT = 720

function convertNameToPath(name)
	local outStr = string.gsub(name, " ", "+")
	return outStr
end

function refreshScreen()
	startSearch()
end

function drawListMoviesOnScreen()
	local pos = 1

	for i,v in pairs(menuDetails) do
		local desc = v
		canvas:attrFont ("Arial", 15,"normal" )
		canvas:drawText(defaultX + 20 ,(defaultY * pos), v.title )
		canvas:drawText(240, (defaultY * pos), desc.year)
		canvas:drawText(440, (defaultY * pos), dtvChannel[i][1]) 
		local tabtleTime =  convertXmltvDate(dtvChannel[i][2])
		canvas:drawText(640, (defaultY * pos),string.format("%s:%s - %s/%s/%s", tabtleTime.hour, tabtleTime.min, tabtleTime.day, tabtleTime.month, tabtleTime.year))
		pos = pos + 1
	end

end

function compareMenuOrder(movie1, movie2)
	if(movie1.rating > movie2.rating) then
		return true
	end
end

-- Channel attributes
dtvChannel = {}
function generateMenu(listOfMovies)
	local posi = 1 
	for c, v in pairs(listOfMovies) do
		for __, k in pairs(v) do
			menuDetails[posi] = movieDescription(k.title)
			dtvChannel[posi] = {c, k.start}
			posi = posi + 1
		end
	end
	--Sort by Imdb Rating
	table.sort(menuDetails, compareMenuOrder)
	
end

function startSearch()
	drawGrid()
	local listMovies = searchMoviesOnMyChannels()
	generateMenu(listMovies)
 	drawListMoviesOnScreen()
	drawMenuSelect(defaultX, defaultY)
	drawSubMenu(menuDetails[currentPosition])
	footerShow(false, false, false, true, false)
	canvas:flush()
end

function drawGrid()
	canvas:attrColor ("white")
	canvas:attrFont ("Arial", 20, "bold")
	canvas:drawText (0,0,"The Best Movies of Day for You")
	canvas:drawText (840, 0, "Details")
	canvas:attrFont ("Arial", 18, "normal")
	canvas:drawText (40,20,  "Title")
	canvas:drawText (240,20, "Year")
	canvas:drawText (440,20, "Channel")
	canvas:drawText (640,20, "Time")
	canvas:drawLine (800, 0, 800, 720)
	canvas:flush()
end

function wrapText(text, max)
	local wrapList = {}
	local loopMax = #text/max + (#text%max)
	for i=1 , loopMax do
		wrapList[i] = string.sub(text, ((i-1) * max) + 1 , i * max)
	end
	return wrapList

end

function drawSubMenu(movie)
	drawFolderOnDisplay(movie.poster, 820, 40)
	canvas:attrColor("white")
	canvas:drawText(1040, 40, string.format("Title: %s", movie.title))
	canvas:drawText(1040, 60, string.format("Year: %s", movie.year))
	canvas:drawText(1040, 80, string.format("RunTime: %s", movie.runtime))
	canvas:drawText(1040, 100, "Directors:")
	
	local line = 0
	for i,v in pairs(movie.directors) do
		canvas:drawText(1040, 120 + (20 * line), "  "..v)
		line = line + 1
	end
	
	canvas:drawText(1040, 120 + (20 * line), "Genres:") 
	for i,v in pairs(movie.genres) do
		line = line + 1
		canvas:drawText(1040, 120 + (20 * line), "  "..v)
	end

	canvas:drawText(1040, 140 + (20 * line), "Writers:")
	for i,v in pairs(movie.writers) do
		line = line +1
		canvas:drawText(1040, 140 + (20 * line), "  "..v)
	end

	canvas:drawText(1040, 160 + (20 * line), "Rating:")
	canvas:drawText(1040, 160 + (20 * (line+1)), "  "..movie.rating)


	canvas:drawText(820, 360, "Story Line")
	local story = wrapText(movie.story,60)
	local lineCounter = 1
	for i,v in pairs(story) do
		if(lineCounter < 15) then
			canvas:drawText(820, 380 + (20 * (i-1)), v)
			lineCounter = lineCounter + 1
		else
			break --Break if the number of lines is biger than the number of free spaces.
		end
	end
	canvas:flush()
end

function drawFolderOnDisplay(imgName,posX, posY)
	local imgCanvas = canvas:new(imgName)
	canvas:compose(posX, posY, imgCanvas)
end

function drawMenuSelect(posX,posY)
	canvas:attrColor("lime")
	canvas:drawText(posX,posY,"->")
	canvas:flush()
end


function reciveEvent(event)
	
	local oldY = defaultY * currentPosition
	local oldX = defaultX
	if(event == "UP") then
		if(currentPosition ~= 1) then
			cleanScreenSection(oldX ,oldY ,oldX ,oldY)
			cleanSubMenu()
			currentPosition = currentPosition - 1
			drawMenuSelect(defaultX, defaultY * currentPosition)
			drawSubMenu(menuDetails[currentPosition])
		end
	elseif (event == "DW") then
		if(currentPosition ~= #menuDetails ) then
			cleanScreenSection(defaultX,oldY,oldX,oldY)
			cleanSubMenu()
			currentPosition = currentPosition + 1
			drawMenuSelect(defaultX, defaultY * currentPosition)
			drawSubMenu(menuDetails[currentPosition])
		end
	elseif (event == "ET") then
		--Do something
	end
end


function preLoading(running)
	if(running) then
		canvas:attrColor("white")
		canvas:attrFont ("Arial" , 50, "bold" )
		canvas:drawText(HEIGHT/4, WIDTH/3, "Loading...")
		canvas:flush()
	else
		cleanScreen()
	end
		
end

function cleanSubMenu()
	canvas:attrColor(0,0,0,0)
	canvas:clear(820,19,1280,648)
	canvas:flush()
end

function cleanScreenSection(x1, y1, x2, y2)
	canvas:attrColor(0,0,0,0)
	canvas:clear(x1,y1,x2,y2)
	canvas:flush()
end

function cleanScreen()
	canvas:attrColor(255,255,255,255)
	canvas:clear()
	canvas:flush()
end
	
